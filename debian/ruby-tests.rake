require 'gem2deb/rake/testtask'
Gem2Deb::Rake::TestTask.new do |test|
  # FIXME enable more of the upstream test suite
  test.pattern = 'test/xsd/test_*.rb'
  test.verbose = true
end
task :default => :test
